import geoip from 'geoip-lite'

import { getLocales } from '_utils/functions/contentfulClient'
import { geoChannels, localeToIso } from '_utils/config/geo'
import { channels } from '_utils/config/geo'

/**
 * @function Get the localized url for the visitor
 *
 * @async
 * @param {object} req - NextJS Server side request object
 * @returns {string} url - Relative url to redirect the visitor
 */
export const getRedirectUrl = async (req) => {
    const requests = [getLocales()]
    const [{ items: locales }] = await Promise.all(requests)

    const acceptLanguage = req.headers['accept-language']

    const ip = req.headers['x-forwarded-for'] || ''
    const { country = '' } = geoip.lookup(ip) || {}

    const channelID = geoChannels[country] || ''

    const url = {
        channel: '',
        locale: '',
    }

    if (channelID) {
        const channel = channels.find(({ name }) => name === channelID)

        const languages = channel.locales.map((code) => code.replace('_', '-'))

        // Reversed to put the more specific country codes first
        let defaultCode = ''
        const { code = defaultCode } =
            locales
                .reverse()
                .filter(({ code }) => languages.includes(code))
                .find(({ code, default: defaultLanguage }) => {
                    if (defaultLanguage) defaultCode = code
                    return acceptLanguage.includes(code)
                }) || {}

        url.channel = channelID.replace('web_b2c_', '')
        url.locale = localeToIso[code]
    }

    if (!url.channel || !url.locale) {
        url.channel = 'int'
        url.locale = 'en'
    }

    return `/${url.channel}/${url.locale}`
}

export const handleRedirect = async (req, res) => {
    const url = await getRedirectUrl(req)

    res.writeHead(307, {
        Location: url,
    })
    res.end()

    return { props: {} }
}
