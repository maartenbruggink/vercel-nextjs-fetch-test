import { createClient } from 'contentful'

const SPACE = process.env.NEXT_PUBLIC_CONTENTFUL_SPACE_ID
const TOKEN = process.env.NEXT_PUBLIC_CONTENTFUL_ACCESS_TOKEN
const ENV = process.env.NEXT_PUBLIC_CONTENTFUL_ENV || 'master'

const client = createClient({
    space: SPACE,
    accessToken: TOKEN,
    environment: ENV,
})

/**
 * @function Get Locales from Contentful
 *
 * @async
 * @returns {object} locales - Contentful response
 */
export async function getLocales() {
    return await client.getLocales()
}

/**
 * @function Make a request to Contentful
 *
 * @async
 * @param {object} query
 * @returns {object} entries - Contentful response
 */
export async function getEntries(query = {}) {
    return await client.getEntries(query)
}
