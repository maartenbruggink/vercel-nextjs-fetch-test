import { clamp, rangeInRange } from '_utils/functions/math'

export const shouldUpdateX = ({ x, inViewBounds, el, minX }) => {
    const elRange = [el.offsetLeft, el.offsetLeft + el.offsetWidth]

    const { withinBounds, margins } = rangeInRange(elRange, inViewBounds)

    if (withinBounds) return { shouldUpdate: false, newX: 0 }

    const { left, right } = margins
    const movX = left < 0 ? left * -1 : right

    return { shouldUpdate: true, newX: clamp(x + movX, minX, 0) }
}
