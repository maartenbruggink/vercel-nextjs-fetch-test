/**
 * @function Format the CF resourceSets to a simple object to store in a context
 *
 * @param {array} resourceSets - CF reply containing all resourceSets
 * @returns {object} translationStrings - Object with translations
 */
export const formatTranslationStrings = (resourceSets = []) =>
    resourceSets.reduce((acc, { fields: { name, resources = [] } }) => {
        acc[name] = resources.reduce((acc, { fields: { key, value } }) => {
            acc[key] = value
            return acc
        }, {})

        return acc
    }, {})
