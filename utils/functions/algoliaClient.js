import algoliasearch from 'algoliasearch'

const ALGOLIA_APP_ID = process.env.NEXT_PUBLIC_ALGOLIA_APP_ID || ''
const ALGOLIA_SEARCH_ONLY_ID =
    process.env.NEXT_PUBLIC_ALGOLIA_SEARCH_ONLY_ID || ''
const ALGOLIA_INDICE_PREFIX =
    process.env.NEXT_PUBLIC_ALGOLIA_INDICE_PREFIX || 'mammut_dev_'
export const algoliaClient = algoliasearch(
    ALGOLIA_APP_ID,
    ALGOLIA_SEARCH_ONLY_ID,
)

export const getIndiceName = (channel = 'eu', locale = 'en', type = '') => {
    channel = (channel !== 'int' && channel) || 'eu'
    locale = locale || 'en'

    return `${ALGOLIA_INDICE_PREFIX}${type}${channel}_${locale}`
}

/**
 * @function Get a indice from Algolia
 * @param channel - The indice channel
 * @param locale - The indice locale
 * @param type
 * @param indice - You can use this param to force an indice
 * @return SearchIndex
 */
export const getIndice = (
    channel = 'eu',
    locale = 'en',
    type = '',
    indice = '',
) =>
    indice
        ? indice
        : algoliaClient.initIndex(getIndiceName(channel, locale, type))

// ONLY SERVERSIDE
/**
 * @function Get Algolia product by SLUG
 * @param slug
 * @param channel
 * @param locale
 * @returns {Promise<boolean|any>}
 */
export const getAlgoliaProduct = async ({ slug, channel, locale }) => {
    const algoliaIndice = getIndice(channel, locale)
    return await algoliaIndice
        .search(slug, {
            restrictSearchableAttributes: ['slug'],
        })
        .then(
            (data) => {
                if (!data.nbHits) {
                    throw new Error(`${slug} not found in Algolia`)
                }
                return data.hits[0]
            },
            (error) => {
                throw new Error(error)
            },
        )
        .catch((error) => {
            console.error(error)
            return null
        })
}

export const getAlgoliaSubcategory = async ({
    gender,
    type,
    filter,
    channel,
    locale,
}) => {
    try {
        if (!gender || !type || !channel || !locale) {
            throw new Error(
                'Gender, type, channel and locale cannot be undefined',
            )
        }

        const requestUrl = `/api/${channel}/${locale}/algolia/menu/${gender}/${type}/${filter}`

        const apiRes = await fetch(requestUrl)

        return await apiRes.json()
    } catch (e) {
        console.error(e)
        return false
    }
}

export const getAlgoliaNavigationTree = async (query) => {
    // TODO: THIS IS BETA AND TEMPORAL UNTIL DECIDE THE BEST SOLUTION FOR THE NAV TREE

    try {
        const menuFilters = await getAlogoliaMenuFilters(query)
        //const activities = await getAlgoliaNavigationLevel(150, query)

        for (let filter in menuFilters) {
            let currentFilter = menuFilters[filter]
            currentFilter['children'] = await getAlgoliaNavigationLevel(
                currentFilter?.objectID,
                query,
            )

            for (let subFilter in currentFilter['children']) {
                currentFilter['children'][subFilter][
                    'children'
                ] = await getAlgoliaNavigationLevel(
                    currentFilter['children'][subFilter]?.objectID,
                    query,
                )
            }

            menuFilters[filter] = currentFilter
        }

        return { menuFilters }
    } catch (e) {
        console.error(e)
        return false
    }
}

const getAlogoliaMenuFilters = async ({ channel, locale }) => {
    const algoliaIndice = getIndice(channel, locale, 'category_')

    const men = await algoliaIndice.getObject('10')
    const women = await algoliaIndice.getObject('20')

    return [men, women]
}

const getAlgoliaNavigationLevel = async (level = '0', { channel, locale }) => {
    channel = (channel !== 'int' && channel) || 'eu'
    locale = locale || 'en'
    const algoliaIndice = getIndice(channel, locale, 'category_')

    return algoliaIndice
        .search(`"${level}"`, {
            restrictSearchableAttributes: ['parent.id'],
        })
        .then(
            (data) => {
                return data?.hits || []
            },
            (error) => {
                console.error(error)
                return []
            },
        )
}
