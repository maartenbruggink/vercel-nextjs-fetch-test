let biaHtmlRef
const keys = [32, 37, 38, 39, 40]

/**
 * Block body scroll by wheel
 *
 * @param {event} e
 */
function blockBodyScroll(e) {
    e = e || window.event
    if (e.preventDefault) e.preventDefault()
    e.returnValue = false
}

/**
 * Block body scroll by keys
 *
 * @param {event} e
 */
function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        e.preventDefault()
        return false
    }
}

/**
 * Check if a device has touch events
 *
 * @returns {boolean}
 */
export function isTouch() {
    try {
        document.createEvent('TouchEvent')
        return true
    } catch (e) {
        return false
    }
}

/**
 * Set overflow on the body element
 *
 * @param {string} newState
 */
export function setHtmlOverflow(newState = 'initial') {
    if (!biaHtmlRef) biaHtmlRef = document.querySelector('html')

    if (isTouch()) {
        if (newState === 'initial') {
            if (!window.BIA_SCROLL_BLOCK) return

            if (window.addEventListener) {
                window.removeEventListener('scroll', blockBodyScroll, false)
                window.removeEventListener('touchmove', blockBodyScroll, false)
            }
            window.onmousewheel = document.onmousewheel = null
            window.ontouchmove = null
            window.onwheel = null
            document.onkeydown = null

            window.BIA_SCROLL_BLOCK = false
        }

        if (newState === 'hidden') {
            if (window.BIA_SCROLL_BLOCK) return
            if (window.removeEventListener) {
                window.addEventListener('scroll', blockBodyScroll, false)
                window.addEventListener('touchmove', blockBodyScroll, false)
            }
            window.onwheel = blockBodyScroll
            window.ontouchmove = blockBodyScroll
            window.onmousewheel = document.onmousewheel = blockBodyScroll
            document.onkeydown = preventDefaultForScrollKeys

            window.BIA_SCROLL_BLOCK = true
        }
    }

    biaHtmlRef.style.overflow = newState
}
