import { channels } from '_utils/config/geo'

/**
 * @function Get the current channel based on the query var
 *
 * @async
 * @param {string} channel - Channel query var
 * @param {object} req - NextJS Server side request object
 * @returns {object} channel - Active channel from the Mammut API
 */
export const getCurrentChannel = (channelQuery) => {
    const channelID = `web_b2c_${channelQuery}`
    return channels.find(({ name }) => name === channelID) || {}
}

/**
 * @function Get the current active locale for Contentful
 *
 * @async
 * @param {object} ctx - NextJS Server side ctx
 * @returns {array} locale - Current Contentful locale
 */
export const getLocale = ({ channel, locale }) => {
    if (channel === 'int' && locale === 'en') return 'en-EU'

    const { locales = [] } = getCurrentChannel(channel)
    const activeLocale = locales.find((channelLocale) =>
        channelLocale.includes(locale),
    )

    if (!activeLocale) return ''

    return activeLocale.replace('_', '-')
}
