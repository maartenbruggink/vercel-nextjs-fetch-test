/**
 * @function  Combine multiple string into one,
 * separated by spaces and filters out the empty strings
 *
 * @params {string} (Multiple) classname - Single classname
 * @returns {string} ClassNames - Combined classnames
 */
export function mergeClassNames(...classNames) {
    return [...classNames].filter((c) => !!c).join(' ')
}

/**
 * Captilize the first letter of a string
 *
 * @param {string} string
 * @returns {string}
 */
export function capitalize(string) {
    if (string.length === 0) return ''
    return string[0].toLocaleUpperCase() + string.substr(1)
}

/**
 * Add a reference to an array
 *
 * @param {element} el
 * @param {array} array
 * @returns {undefined}
 */
export function refToArray(el, array) {
    if (!el || !Array.isArray(array) || array.includes(el)) return
    return array.push(el)
}
