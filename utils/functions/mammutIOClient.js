export const MAMMUT_IO_URL = process.env.MAMMUT_IO_URL || ''
export const NEXT_API_URL = process.env.NEXT_API_URL || ''

/**
 * @function Returns the headers for Mammut IO API defined in ENV
 * @returns {{'Ocp-Apim-Subscription-Key': string, 'Content-Type': string}}
 */
export const getAPIHeaders = () => {
    const key = process.env.MAMMUT_IO_KEY || ''
    const headers = {
        'Ocp-Apim-Subscription-Key': key,
        'Content-Type': 'application/json',
    }

    return headers
}

export const getLanguageFromChannel = (channel) => {
    let channelFragments = channel.split('_')

    if (!channel || channelFragments.length <= 1) return 'EU'

    return channelFragments[channelFragments.length - 1].toUpperCase()
}

/**
 * @function Get products by using categoryID
 * @param categoryId
 * @param channel
 * @param locale
 * @param options
 * @returns {Promise<boolean|any>}
 */

//TODO: Will Change to ALGOLIA
export const getProductsByCategory = async ({
    categoryId,
    channel,
    locale,
    options = {},
}) => {
    try {
        if (!categoryId || !channel || !locale) {
            throw new Error(
                'CategoryId, channel and locale cannot be undefined',
            )
        }

        const requestUrl = `/api/${channel}/${locale}/mammutIO/category/${categoryId}`

        const apiRes = await fetch(requestUrl)

        return await apiRes.json()
    } catch (e) {
        console.error(e)
        return false
    }
}

/**
 * @function Get product by SKU
 * @param sku
 * @param channel
 * @param locale
 * @returns {Promise<boolean|any>}
 */
export const getProductBySku = async ({ sku, channel, locale }) => {
    try {
        if (!sku || !channel || !locale) {
            throw new Error('Sku, channel and locale cannot be undefined')
        }

        const requestUrl = `/api/${channel}/${locale}/mammutIO/product/${sku}`

        const apiRes = await fetch(requestUrl)

        return await apiRes.json()
    } catch (e) {
        console.error(e)
        return false
    }
}
