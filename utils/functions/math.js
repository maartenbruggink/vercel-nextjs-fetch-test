/**
 * @function Get the difference between two numbers
 *
 * @param {number} num1
 * @param {number} num2
 * @returns {number}
 */
export function diff(num1, num2) {
    return num1 > num2 ? num1 - num2 : num2 - num1
}

/**
 * @function Clamp a number between two numbers
 *
 * @param {number} value
 * @param {number} min
 * @param {number} max
 * @returns {number}
 */
export function clamp(value, min, max) {
    return Math.min(max, Math.max(min, value))
}

/**
 * @function Map a number between two numbers to two different numbers
 *
 * @param {array} from
 * @param {array} to
 * @param {number} s
 * @returns {number}
 */
export function map(from, to, s) {
    return to[0] + ((s - from[0]) * (to[1] - to[0])) / (from[1] - from[0])
}

/**
 * @function Map a number between two numbers to two different numbers
 *
 * @param {number} num
 * @param {number} (Optional) maxPrecision
 * @returns {number} rounded num
 */
export function round(num = 0, maxPrecision = 100) {
    return Math.floor(num * maxPrecision) / maxPrecision
}

/**
 * @function Check if a range is within bounds
 *
 * @param {array} range - Array with two number [ min, max ]
 * @param {array} bounds - Array with two number [ min, max ]
 * @returns {object} withinBounds, margins
 */
export function rangeInRange(range = [], bounds = []) {
    const [left, right] = range
    const [bLeft, bRight] = bounds

    const fitsLeft = left >= bLeft && left < bRight
    const fitsRight = right > bLeft && right <= bRight

    const margins = {
        left: left - bLeft,
        right: bRight - right,
    }

    return {
        withinBounds: fitsLeft && fitsRight,
        margins,
    }
}
