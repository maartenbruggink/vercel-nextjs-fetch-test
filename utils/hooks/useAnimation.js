import { useCallback, useEffect } from 'react'

const callbacks = new Set()
let requestID
const animate = () => {
    callbacks.forEach((callback) => {
        callback()
    })
    requestID = window.requestAnimationFrame(animate)
}

const checkShouldAnimate = () => {
    const shouldAnimate = callbacks.size > 0

    if (shouldAnimate && !requestID) {
        requestID = window.requestAnimationFrame(animate)
    }
    if (!shouldAnimate && requestID) {
        window.cancelAnimationFrame(requestID)
        requestID = undefined
    }
}

const useAnimation = (callback, dependencies = [], active = true) => {
    const func = useCallback(callback, dependencies)

    useEffect(() => {
        if (typeof callback === 'function' && active) {
            callbacks.add(func)
            checkShouldAnimate()

            return () => {
                callbacks.delete(func)
                checkShouldAnimate()
            }
        }
    }, [func, active])
}

export default useAnimation
