import { useState, useEffect } from 'react'

const setters = new Set()
let isHooked = false
const handleResize = () => {
    setters.forEach((setter) => {
        setter({
            width: window.innerWidth,
            height: window.innerHeight,
        })
    })
}

/**
 * @hook Get the window width and height, SSR returns empty object
 *
 * @returns {object} size - contains window width and height
 */
const useWindowSize = () => {
    const [size, setSize] = useState({})

    useEffect(() => {
        setters.add(setSize)

        if (!isHooked) {
            window.addEventListener('resize', handleResize)
            isHooked = true
        }

        setSize({
            width: window.innerWidth,
            height: window.innerHeight,
        })

        return () => {
            setters.delete(setSize)

            if (setters.size === 0) {
                window.removeEventListener('resize', handleResize)
                isHooked = false
            }
        }
    }, [])

    return size
}

export default useWindowSize
