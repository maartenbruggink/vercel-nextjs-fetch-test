import { useContext } from 'react'

import { TranslationsContext } from '_utils/contexts'

/**
 * @hook Get translation string
 *
 * @param {string} setKey - Key of the set where translations are located
 * @returns {object} set - All the translations in the requested set
 */
export const useTranslation = (setKey = '') => {
    const data = useContext(TranslationsContext)
    return (data && data[setKey]) || {}
}
