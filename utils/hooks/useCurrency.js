import { useContext } from 'react'
import { useRouter } from 'next/router'

import { ChannelsContext } from '_utils/contexts'

export const useCurrency = () => {
    const { channels } = useContext(ChannelsContext)
    const {
        query: { channel = '' },
    } = useRouter()

    if (channel === 'int') return ''

    const channelID = `web_b2c_${channel}`
    const currentChannel = channels.find(({ name }) => name === channelID)
    const {
        currencies: [currency],
    } = currentChannel || { currencies: [] }

    return currency
}
