import { useCallback, useEffect } from 'react'

const functionsToRun = new Set()
let isHooked = false

let throttled = false
const delay = 500
let forLastExec

const runEvents = (isLastTick = false) => {
    functionsToRun.forEach((setter) => {
        setter({ isLastTick })
    })
}

const handleResize = (e) => {
    if (!throttled) {
        runEvents()

        throttled = true

        setTimeout(() => {
            throttled = false
        }, delay)
    }

    clearTimeout(forLastExec)

    forLastExec = setTimeout(() => {
        runEvents(true)
    }, delay)
}

/**
 * @hook Hit a callback on window resize
 *
 * @returns {object} size - contains window width and height
 */
const useResize = (callback, dependencies) => {
    const func = useCallback(callback, dependencies)

    useEffect(() => {
        functionsToRun.add(func)

        if (!isHooked) {
            window.addEventListener('resize', handleResize)
            isHooked = true
        }
        return () => {
            functionsToRun.delete(func)

            if (functionsToRun.size === 0) {
                window.removeEventListener('resize', handleResize)
                isHooked = false
            }
        }
    }, [func])
}

export default useResize
