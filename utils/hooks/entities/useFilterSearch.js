import useSWR from 'swr'
import { useRouter } from 'next/router'

/**
 * @hook Get category entity data from an API
 *
 * @returns {object} entity - Object containing data
 */
const useFilterSearch = ({ filter, search }) => {
    const { query } = useRouter()

    const { channel = '', locale = '' } = query
    const url = `/api/${channel}/${locale}/algolia/${search}`

    let filterParams = ''
    if (filter.name && filter.value) {
        filterParams += `?filterName=${filter.name}&filterValue=${filter.value}`
    }

    const { data, error } = useSWR(url + filterParams, async (url) => {
        const apiRes = await fetch(url)
        return await apiRes.json()
    })

    return data?.hits || []
}

export default useFilterSearch
