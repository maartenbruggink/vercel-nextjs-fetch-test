import useSWR from 'swr'
import { useRouter } from 'next/router'

/**
 * @hook Get variants entity data from an API
 *
 * @returns {object} entity - Object containing data
 */
const useEntityProductVariants = (product) => {
    const { query } = useRouter()
    const id = product?.parent || product?.objectID

    const { channel = '', locale = '' } = query
    const url = `/api/${channel}/${locale}/algolia/product/variants/${id}`

    const { data } = useSWR(url, async (url) => {
        if (!id) return false
        const apiRes = await fetch(url)
        return await apiRes.json()
    })

    return data
}

export default useEntityProductVariants
