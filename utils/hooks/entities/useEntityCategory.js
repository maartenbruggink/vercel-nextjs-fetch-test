import useSWR from 'swr'
import { useRouter } from 'next/router'

/**
 * @hook Get category entity data from an API
 *
 * @returns {object} entity - Object containing data
 */
const useEntityCategory = (category) => {
    const { query } = useRouter()
    const { mammutIoCategoryId: id = '' } = category?.fields || {}

    let { channel = '', locale = '' } = query
    channel = (channel !== 'int' && channel) || 'eu'
    locale = locale || 'en'

    const url = `/api/${channel}/${locale}/algolia/category/${id}`

    const { data, error } = useSWR(url, async (url) => {
        if (!id) return false
        const apiRes = await fetch(url)
        return await apiRes.json()
    })

    return data
}

export default useEntityCategory
