import useSWR from 'swr'
import { useRouter } from 'next/router'
import { getEntries } from '_utils/functions/contentfulClient'

import { getLocale } from '_utils/functions/geoHelpers'

const fetchDataAlgolia = (product, { channel, locale }) => async () => {
    const { mammutIoProductId: sku = false } = product.fields

    if (!sku) return false
    const requestUrl = `/api/${channel}/${locale}/algolia/product/${sku}`

    const apiRes = await fetch(requestUrl)
    return await apiRes.json()
}

const fetchDataCMS = ({ objectID }, query) => async () => {
    const entries = await getEntries({
        content_type: 'product',
        'fields.mammutIoProductId': objectID,
        locale: getLocale(query),
    })

    const { items } = entries || {}
    const [product] = items || []

    return product
}

/**
 * @hook Get product entity data from an API
 *
 * @returns {object} entity - Object containing data
 */
const useEntityProduct = (product) => {
    const isCMSProduct = product.hasOwnProperty('fields')

    const { query } = useRouter()
    const slug = isCMSProduct ? product?.fields?.slug : `io-${product.slug}`

    const urlArr = [...Object.values(query), slug]
    const url = urlArr.join('/')

    const fetcher = isCMSProduct
        ? fetchDataAlgolia(product, query)
        : fetchDataCMS(product, query)

    const { data } = useSWR(url, fetcher)
    return data
}

export default useEntityProduct
