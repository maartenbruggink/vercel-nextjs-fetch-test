/**
 * @constant
 * @type {object}
 */
export const colors = {
    red: '#e2001a',
    white: '#fff',
    black: '#000',
    purple: '#23293C',
    orange: '#F96B05',
    gray: '#E5E5E5',
    transparent: 'transparent',
}
