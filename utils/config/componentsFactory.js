import Typography from '_components/dev/Typography.factory'

import Logo from '_components/svg/Logo.factory'
import LogoIcon from '_components/svg/LogoIcon.factory'
import IconsFactory from '_components/svg/Icons.factory'

import TextMaskFactory from '_components/elements/TextMask.factory'
import TextTypeFactory from '_components/elements/TextType.factory'

import ButtonFactory from '_components/form/Button.factory'
import SelectSliderFactory from '_components/global/SelectSlider.factory'
import BreadcrumbsFactory from '_components/global/Breadcrumbs.factory'

import ProductTooltip from '_components/product/ProductTooltip.factory'
import ProductCard from '_components/product/ProductCard.factory'

import CategorySliderFactory from '_components/categories/CategorySlider.factory'
import CategoryCardFactory from '_components/categories/CategoryCard.factory'

import BlockHeroFactory from '_components/blocks/BlockHero.factory'

export const components = {
    General: {
        Typography: Typography,
    },
    Svg: {
        Logo: Logo,
        LogoIcon: LogoIcon,
        Icons: IconsFactory,
    },
    Text: {
        TextMask: TextMaskFactory,
        TextType: TextTypeFactory,
    },
    Inputs: {
        Button: ButtonFactory,
        SelectSlider: SelectSliderFactory,
        Breadcrumbs: BreadcrumbsFactory,
    },
    Products: {
        Tooltip: ProductTooltip,
        ProductCard: ProductCard,
    },
    Categories: {
        CategorySlider: CategorySliderFactory,
        CategoryCard: CategoryCardFactory,
    },
    Layouts: {
        BlockHero: BlockHeroFactory,
    },
}
