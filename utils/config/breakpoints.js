/**
 * @constant
 * @type {object}
 */
export const breakpoints = {
    xsmall: 600,
    small: 768,
    medium: 1024,
    large: 1200,
    xlarge: 1400,
    xxlarge: 1600,
}
