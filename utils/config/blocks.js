import BlockHero from '_components/blocks/BlockHero'
import BlockCategories from '_components/blocks/BlockCategories'
import BlockSlideshow from '_components/blocks/BlockSlideshow'
import BlockEntityColumns from '_components/blocks/BlockEntityColumns'

export const blocksMap = {
    blockHero: BlockHero,
    blockCategories: BlockCategories,
    blockSlideshow: BlockSlideshow,
    blockEntityColumns: BlockEntityColumns,
}

export const BlockNotFound = (block) => {
    console.error(`Block type for ${block.sys.id} not found.`, block)
    return <></>
}
