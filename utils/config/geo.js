/**
 * @constant
 * @type {object}
 */
export const localeToIso = {
    'en-EU': 'en',
    'en-GB': 'en',
    'en-US': 'en',
    'fr-FR': 'fr',
    'de-DE': 'de',
}

/**
 * @constant
 * @type {object}
 */
export const channels = [
    {
        name: 'web_b2c_fr',
        catalog: 'mammut',
        currencies: ['EUR'],
        locales: ['fr_FR'],
    },
    {
        name: 'web_b2c_us',
        catalog: 'mammut',
        currencies: ['USD'],
        locales: ['en_US'],
    },
    {
        name: 'web_b2c_eu',
        catalog: 'mammut',
        currencies: ['EUR'],
        locales: ['en_EU'],
    },
    {
        name: 'web_b2c_de',
        catalog: 'mammut',
        currencies: ['EUR'],
        locales: ['de_DE'],
    },
    {
        name: 'web_b2c_no',
        catalog: 'mammut',
        currencies: ['NOK'],
        locales: ['en_EU'],
    },
    {
        name: 'web_b2c_at',
        catalog: 'mammut',
        currencies: ['EUR'],
        locales: ['de_DE'],
    },
    {
        name: 'web_b2c_ch',
        catalog: 'mammut',
        currencies: ['CHF'],
        locales: ['de_DE', 'fr_FR'],
    },
    {
        name: 'web_b2c_uk',
        catalog: 'mammut',
        currencies: ['GBP'],
        locales: ['en_GB'],
    },
]

/**
 * @constant
 * @type {object}
 */
export const geoChannels = {
    // Europe / specific
    CH: 'web_b2c_ch',
    DE: 'web_b2c_de',
    AT: 'web_b2c_at',
    FR: 'web_b2c_fr',
    NO: 'web_b2c_no',

    // Europe / generic
    BE: 'web_b2c_eu',
    CZ: 'web_b2c_eu',
    DK: 'web_b2c_eu',
    IT: 'web_b2c_eu',
    NL: 'web_b2c_eu',
    PO: 'web_b2c_eu',
    SK: 'web_b2c_eu',
    SI: 'web_b2c_eu',
    SE: 'web_b2c_eu',

    // Others
    GB: 'web_b2c_uk',
    UK: 'web_b2c_uk',
    US: 'web_b2c_us',

    // Wildcard
    INT: 'int',
}
