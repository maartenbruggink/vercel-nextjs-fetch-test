import { createStore } from 'redux'

import reducers from './reducers'

const excludeFromPersisted = ['appReducer']

/**
 * Get Store
 * @param  {} persistedState={}
 * @returns {} store
 */
export default function getStore(persistedState = {}) {
    // In the client, try to get data from the server and/or local storage
    if (typeof document !== `undefined`) {
        // 1. Check if the server set data object is there
        if (window.__PRELOADED_STATE__) {
            // Merge default object with server pushed data
            persistedState = {
                ...persistedState,
                ...window.__PRELOADED_STATE__,
            }

            // Remove the server pushed data elements
            const scriptContainer = document.getElementById(`preloadedState`)
            scriptContainer.remove && scriptContainer.remove()
            delete window.__PRELOADED_STATE__
        }

        // 2. Check if there is data in the local storage
        const storage = window.localStorage
        const reduxState = storage.getItem(`reduxState`)

        if (reduxState) {
            const localData = JSON.parse(reduxState)

            if (localData) {
                // Merge local data with server pushed data
                persistedState = {
                    ...localData,
                    ...persistedState,
                }
            }
        }
    }

    // Create the store
    const store = createStore(reducers, persistedState)

    // On value change save the data to local storage
    // Debounce this for performance reasons
    if (typeof document !== `undefined`) {
        let timeout

        const saveToLocalStorage = () => {
            const storeData = store.getState()
            const persistedStateObject = {}

            Object.entries(storeData).forEach(([key, value]) => {
                if (excludeFromPersisted.includes(key)) return
                persistedStateObject[key] = value
            })

            window.localStorage.setItem(
                `reduxState`,
                JSON.stringify(persistedStateObject),
            )
        }

        store.subscribe(() => {
            const later = () => {
                timeout = null
                saveToLocalStorage()
            }
            clearTimeout(timeout)
            timeout = setTimeout(later, 2000)
        })
    }

    return store
}
