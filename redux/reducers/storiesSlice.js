import { createSlice } from '@reduxjs/toolkit'

const storiesSlice = createSlice({
    name: 'stories',
    initialState: [],
    reducers: {
        addStory(state, action) {
            const { id, text } = action.payload
            state.push({ id, text })
        },
        removeStory(state, action) {
            const storyIndex = state.indexOf(
                (story) => story.id === action.payload,
            )
            if (storyIndex >= 0) state.slice(storyIndex, 1)
        },
    },
})

export const { addStory, removeStory } = storiesSlice.actions

export default storiesSlice.reducer
