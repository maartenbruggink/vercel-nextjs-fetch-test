import { createSlice } from '@reduxjs/toolkit'

const appSlice = createSlice({
    name: 'app',
    initialState: {
        navOpen: '',
        menuTheme: 'dark',
    },
    reducers: {
        setMenuTheme: (state, action) => {
            state.menuTheme = action.payload
        },
        toggleNav: (state, action) => {
            state.navOpen =
                action.payload === state.navOpen ? '' : action.payload
        },
    },
})

export const { setMenuTheme, toggleNav } = appSlice.actions

export default appSlice.reducer
