import { combineReducers } from 'redux'

import appReducer from './reducers/appSlice'
import storiesReducer from './reducers/storiesSlice'

const allReducers = combineReducers({
    appReducer,
    storiesReducer,
})

export default (state, action) => allReducers(state, action)
