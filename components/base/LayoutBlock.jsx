import { colors } from '_utils/config/vars'
import s from './LayoutBlock.module.scss'
import { blocksMap, BlockNotFound } from '_utils/config/blocks'
import { mergeClassNames } from '_utils/functions/templateHelpers'
import Log from '_components/dev/Log'

const LayoutBlock = (props) => {
    const { fields } = props

    const color = fields.color || 'transparent'
    const textColor = fields.textColor || 'Dark'

    return (
        <section
            className={mergeClassNames(s.container, s[textColor.toLowerCase()])}
            style={{ backgroundColor: colors[color.toLowerCase()] }}
        >
            {fields.block && renderBlock(fields.block)}
            <Log>
                <h6>[Block] {fields.layoutName}</h6>
                <div>Text Color: {textColor}</div>
                <div>BG Color: {color}</div>
            </Log>
        </section>
    )
}

const renderBlock = (block) => {
    const { contentType } = block.sys
    const BlockComponent = blocksMap[contentType.sys.id] || BlockNotFound

    return (
        <div className={s.block}>
            <BlockComponent {...block} />
        </div>
    )
}

export default LayoutBlock
