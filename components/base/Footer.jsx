import RegionSwitch from '_components/base/RegionSwitch'
import LocaleSwitch from '_components/base/LocaleSwitch'
import { useCurrency } from '_utils/hooks/useCurrency'

const Footer = () => {
    const currency = useCurrency()
    return (
        <footer style={{ margin: '3em 0', padding: '0 2em' }}>
            <RegionSwitch />
            <LocaleSwitch />
            <span>
                {currency
                    ? `Prices in ${currency}`
                    : `We don't sell to your country yet`}
            </span>
        </footer>
    )
}

export default Footer
