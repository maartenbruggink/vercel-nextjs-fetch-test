import { useContext } from 'react'
import { useRouter } from 'next/router'

import { ChannelsContext } from '_utils/contexts'
import { localeToIso } from '_utils/config/geo'

const LocaleSwitch = () => {
    const { channels, locales: cfLocales } = useContext(ChannelsContext)
    const { query, pathname } = useRouter()

    const basePath = Object.entries(query).reduce((acc, [key, val]) => {
        if (key === 'locale') return acc
        return acc.replace(`[${key}]`, val)
    }, pathname)

    const currentChannelKey = `web_b2c_${query.channel}`
    let { locales: mammutLocales = [] } =
        channels.find(({ name }) => name === currentChannelKey) || {}

    mammutLocales = [...mammutLocales].map((code) => code.replace('_', '-'))

    if (query.channel === 'int') mammutLocales.push('en-EU')

    return (
        <nav>
            <ul style={{ display: 'flex' }}>
                {cfLocales
                    .filter(({ code }) => mammutLocales.includes(code))
                    .map(({ code }) => {
                        const key = localeToIso[code]

                        return (
                            <li key={code} style={{ marginRight: '1em' }}>
                                {key === query.locale ? (
                                    key
                                ) : (
                                    <a href={basePath.replace(`[locale]`, key)}>
                                       {key}
                                    </a>
                                )}
                            </li>
                        )
                    })}
            </ul>
        </nav>
    )
}

export default LocaleSwitch
