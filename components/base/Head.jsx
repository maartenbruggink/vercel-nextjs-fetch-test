import Head from 'next/head'

export default ({ title = '', metaDescription = '' }) => (
    <Head>
        <title>
            {process.env.NEXT_PUBLIC_APP_NAME} - {title}
        </title>
        <meta name="description" content={metaDescription} />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
)
