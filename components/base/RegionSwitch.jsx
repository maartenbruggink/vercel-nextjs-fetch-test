import { useContext } from 'react'
import { useRouter } from 'next/router'

import { ChannelsContext } from '_utils/contexts'
import { localeToIso } from '_utils/config/geo'

const RegionSwitch = () => {
    const { channels } = useContext(ChannelsContext)
    const { query, pathname } = useRouter()

    const basePath = Object.entries(query).reduce((acc, [key, val]) => {
        if (key === 'channel' || key === 'locale') return acc
        return acc.replace(`[${key}]`, val)
    }, pathname)

    return (
        <nav>
            <ul style={{ display: 'flex', marginBottom: '1em' }}>
                {channels.map(({ name, locales }) => {
                    const key = name.replace('web_b2c_', '')
                    const channelPath = basePath.replace(`[channel]`, key)
                    const [mainLanguage] = locales
                    const locale = localeToIso[mainLanguage.replace('_', '-')]

                    return (
                        <li key={key} style={{ marginRight: '1em' }}>
                            {key === query.channel ? (
                                key
                            ) : (
                                <a
                                    href={channelPath.replace(
                                        `[locale]`,
                                        locale,
                                    )}
                                >
                                    {key}
                                </a>
                            )}
                        </li>
                    )
                })}

                <li style={{ marginRight: '1em' }}>
                    <a
                        href={basePath
                            .replace(`[channel]`, 'int')
                            .replace(`[locale]`, 'en')}
                    >
                        {'int'}
                    </a>
                </li>
            </ul>
        </nav>
    )
}

export default RegionSwitch
