import { Provider } from 'react-redux'

import { ChannelsContext, TranslationsContext } from '_utils/contexts'

import getStore from '../../redux/store'

const store = getStore()

const DataWrapper = ({ channels, locales, translationStrings, children }) => (
    <Provider store={store}>
        <ChannelsContext.Provider value={{ channels, locales }}>
            <TranslationsContext.Provider value={translationStrings}>
                {children}
            </TranslationsContext.Provider>
        </ChannelsContext.Provider>
    </Provider>
)

export default DataWrapper
