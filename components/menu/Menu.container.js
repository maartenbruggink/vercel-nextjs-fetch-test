import { connect } from 'react-redux'

import { toggleNav } from '_actions'

//  Get data from redux
const mapStateToProps = (state) => ({
    getNavOpen: () => state.appReducer.navOpen,
    getMenuTheme: () => state.appReducer.menuTheme || 'dark',
})

//  Store data in redux
const mapDispatchToProps = { toggleNav }

export default (Comp) => connect(mapStateToProps, mapDispatchToProps)(Comp)
