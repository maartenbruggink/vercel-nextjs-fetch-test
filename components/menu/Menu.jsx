import { mergeClassNames } from '_utils/functions/templateHelpers'

import container from './Menu.container'
import s from './Menu.module.scss'

const Menu = ({
    getNavOpen,
    getMenuTheme,
    toggleNav,
    navSite = {},
    navShop = {},
    menuFilters = [],
}) => {
    return <menu className={mergeClassNames(s.container)}>{'Menu'}</menu>
}

export default container(Menu)
