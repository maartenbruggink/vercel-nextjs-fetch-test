import { useRouter } from 'next/router'

import Link from '_components/links/Link'

const PageLink = ({ page = '', ...linkProps }) => {
    const { query } = useRouter()
    const { channel, locale } = query || {}

    return (
        <Link
            {...linkProps}
            href={`/[channel]/[locale]${page}`}
            as={`/${channel}/${locale}${page}`}
        />
    )
}

export default PageLink
