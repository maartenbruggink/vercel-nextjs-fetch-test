import NextLink from 'next/link'

const Link = (props) => <NextLink {...props} scroll={false} />

export default Link
