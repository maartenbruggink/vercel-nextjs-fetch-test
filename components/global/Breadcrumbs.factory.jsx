import Breadcrumbs from './Breadcrumbs'

const BreadcrumbsFactory = () => {
    return (
        <>
            <h1>{'BreadCrumbs'}</h1>
            <h2>{'Default'}</h2>
            <div
                style={{
                    width: '100%',
                    display: 'flex',
                    justifyContent: 'center',
                    padding: '3vw 0',
                }}
            >
                <Breadcrumbs
                    items={[
                        { slug: '/clothing', label: 'Clothing' },
                        { slug: '/clothing/jackets', label: 'Jackets' },
                        { slug: '/clothing/jackets/hiking', label: 'Hiking' },
                    ]}
                />
            </div>
            <pre>{`
<Breadcrumbs
    items={[
        { slug: '/clothing', label: 'Clothing' },
        { slug: '/clothing/jackets', label: 'Jackets' },
        { slug: '/clothing/jackets/hiking', label: 'Hiking' },
    ]}
/>
        `}</pre>
            <h2>{'A ton'}</h2>
            <div
                style={{
                    width: '100%',
                    display: 'flex',
                    justifyContent: 'center',
                    padding: '3vw 0',
                }}
            >
                <Breadcrumbs
                    items={[
                        { slug: '/one', label: 'One' },
                        { slug: '/two', label: 'Two' },
                        { slug: '/three', label: 'Three' },
                        { slug: '/four', label: 'Four' },
                        { slug: '/five', label: 'Five' },
                        { slug: '/six', label: 'Six' },
                        { slug: '/seven', label: 'Seven' },
                        { slug: '/eight', label: 'Eight' },
                        { slug: '/nine', label: 'Nine' },
                        { slug: '/ten', label: 'Ten' },
                    ]}
                />
            </div>
            <pre>{`
<Breadcrumbs
    items={[
        { slug: '/one', label: 'One' },
        { slug: '/two', label: 'Two' },
        { slug: '/three', label: 'Three' },
        { slug: '/four', label: 'Four' },
        { slug: '/five', label: 'Five' },
        { slug: '/six', label: 'Six' },
        { slug: '/seven', label: 'Seven' },
        { slug: '/eight', label: 'Eight' },
        { slug: '/nine', label: 'Nine' },
        { slug: '/ten', label: 'Ten' },
    ]}
/>
        `}</pre>
            <h2>{'Single entry'}</h2>
            <div
                style={{
                    width: '100%',
                    display: 'flex',
                    justifyContent: 'center',
                    padding: '3vw 0',
                }}
            >
                <Breadcrumbs items={[{ slug: '/single', label: 'Single' }]} />
            </div>
            <pre>{`
<Breadcrumbs
    items={[
        { slug: '/one', label: 'One' },
    ]}
/>
        `}</pre>
        </>
    )
}

export default BreadcrumbsFactory
