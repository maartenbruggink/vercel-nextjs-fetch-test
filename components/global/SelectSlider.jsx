import { useState, useRef, useEffect } from 'react'
import { motion, useSpring } from 'framer-motion'

import { mergeClassNames } from '_utils/functions/templateHelpers'
import { delay } from '_utils/functions/delay'
import { shouldUpdateX } from '_utils/functions/sliderHelpers'

import Button from '_components/form/Button'

import s from './SelectSlider.module.scss'

const SelectSlider = ({
    className,
    value: currentValue,
    items = [],
    onClick,
    ...props
}) => {
    const [leftConstraint, setLeftConstraint] = useState(0)
    const itemEl = useRef()
    const isDragging = useRef(false)

    const x = useSpring(0, {
        damping: 150,
        mass: 2,
        stiffness: 500,
    })

    const disableClick = () => (isDragging.current = true)
    const enableClick = async () => {
        await delay(10)
        isDragging.current = false
    }

    const handleFocus = (i) => () => {
        if (leftConstraint >= 0 || !itemEl.current) return
        const children = [...itemEl.current.children]
        const el = children[i]

        const curX = x.get()
        const viewLeft = curX * -1

        const { shouldUpdate, newX } = shouldUpdateX({
            x: curX,
            inViewBounds: [viewLeft, viewLeft + itemEl.current.offsetWidth],
            el,
            minX: leftConstraint,
        })

        if (!shouldUpdate) return
        x.set(newX)
    }

    const handleOnClick = (value) => () => {
        if (isDragging.current) return
        onClick && onClick(value)
    }

    useEffect(() => {
        if (itemEl.current) {
            const children = [...itemEl.current.children]
            const totalWidth = children.reduce(
                (acc, item) => (acc += item.offsetWidth),
                0,
            )
            const newLeftConstraint = itemEl.current.offsetWidth - totalWidth

            if (newLeftConstraint !== leftConstraint) {
                setLeftConstraint(newLeftConstraint)
            }
        }
    }, [currentValue])

    let innerProps = {}

    if (leftConstraint < 0) {
        innerProps = {
            drag: 'x',
            style: { x },
            dragConstraints: { right: 0, left: leftConstraint },
            onDragStart: disableClick,
            onDragEnd: enableClick,
        }
    }

    return (
        <div className={mergeClassNames(s.container, className)} {...props}>
            <motion.div className={s.inner} ref={itemEl} {...innerProps}>
                {items.map(({ label, value }, i) => {
                    const active = currentValue === value

                    const props = {
                        onFocus: handleFocus(i),
                    }

                    return active ? (
                        <Button
                            key={value}
                            className={mergeClassNames(s.option)}
                            label={label}
                            {...props}
                        />
                    ) : (
                        <button
                            key={value}
                            className={mergeClassNames(s.option, s.passive)}
                            onClick={handleOnClick(value)}
                            {...props}
                        >
                            {label}
                        </button>
                    )
                })}
            </motion.div>
        </div>
    )
}

export default SelectSlider
