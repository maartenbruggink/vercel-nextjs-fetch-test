import { Fragment } from 'react'

import { mergeClassNames } from '_utils/functions/templateHelpers'

import PageLink from '_components/links/PageLink'
import Arrow from '_components/svg/Arrow'

import s from './Breadcrumbs.module.scss'
import Button from '_components/form/Button'

const Breadcrumbs = ({ className, items = [], ...props }) => {
    if (!items || items.length === 0) return null
    const [final, ...rest] = [...items].reverse()
    const links = (rest || []).reverse()

    return (
        <div className={mergeClassNames(s.container, className)} {...props}>
            {links.map(({ slug, label }) => (
                <Fragment key={slug}>
                    <PageLink page={slug}>
                        <a className={s.link}>{label}</a>
                    </PageLink>
                    <Arrow className={s.icon} />
                </Fragment>
            ))}
            <Button className={s.active} label={final.label} />
        </div>
    )
}

export default Breadcrumbs
