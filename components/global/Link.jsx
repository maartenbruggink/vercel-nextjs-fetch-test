import { mergeClassNames } from '_utils/functions/templateHelpers'

import PageLink from '_components/links/PageLink'
import CategoryLink from '_components/links/CategoryLink'
import ProductLink from '_components/links/ProductLink'
import Arrow from '_components/svg/Arrow'

import s from './Link.module.scss'

const linkMap = {
    page: PageLink,
    category: CategoryLink,
    product: ProductLink,
}

const slugMap = {
    page: (slug) => ({ page: slug }),
    category: (slug) => ({ category: slug }),
    product: (slug) => ({ product: slug }),
}

const Link = ({ className, data = {}, ...props }) => {
    const { title, label, slug, linkType } = data?.fields || {}

    const Link = linkMap[linkType] || PageLink
    const slugModifier = slugMap[linkType] || slugMap.page

    return (
        <Link {...slugModifier(slug)}>
            <a className={mergeClassNames(s.container, className)} {...props}>
                <Arrow className={s.icon} />
                {label || title}
            </a>
        </Link>
    )
}

export default Link
