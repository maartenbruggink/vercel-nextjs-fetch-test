import { useState } from 'react'

import SelectSlider from './SelectSlider'
import { capitalize } from '_utils/functions/templateHelpers'

const options = [
    'one',
    'two',
    'three',
    'four',
    'five',
    'six',
    'seven',
    'eight',
    'nine',
    'ten',
]

const SelectSliderFactory = () => {
    const [oneValue, oneSetValue] = useState('one')
    const [twoValue, twoSetValue] = useState('one')

    const oneHandleClick = (val) => oneSetValue(val)
    const twoHandleClick = (val) => twoSetValue(val)

    return (
        <>
            <h1>{'SelectSlider'}</h1>
            <h2>{'Default'}</h2>
            <div
                style={{
                    width: '100%',
                    display: 'flex',
                    justifyContent: 'center',
                    padding: '3vw 0',
                }}
            >
                <SelectSlider
                    value={oneValue}
                    onClick={oneHandleClick}
                    items={options.map((value) => ({
                        value,
                        label: capitalize(value),
                    }))}
                />
            </div>
            <pre>{`
<SelectSlider
    value={value}
    onClick={handleClick}
    items={options}
/>
        `}</pre>
            <h2>{'Overflowing'}</h2>
            <div
                style={{
                    width: '400px',
                    display: 'flex',
                    justifyContent: 'center',
                    padding: '3vw 0',
                }}
            >
                <SelectSlider
                    value={twoValue}
                    onClick={twoHandleClick}
                    items={options.map((value) => ({
                        value,
                        label: capitalize(value),
                    }))}
                />
            </div>
            <pre>{`
<SelectSlider
    value={value}
    onClick={handleClick}
    items={options}
/>
        `}</pre>
        </>
    )
}

export default SelectSliderFactory
