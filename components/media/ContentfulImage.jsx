import PropTypes from 'prop-types'
import { useInView } from 'react-intersection-observer'

import { mergeClassNames } from '_utils/functions/templateHelpers'
import { round } from '_utils/functions/math'

import s from './ContentfulImage.module.scss'

const getImgFields = (asset) => {
    const { id: type = '' } = asset?.sys?.contentType?.sys || {}
    const fields = asset?.fields || {}

    if (type === 'imageWithFocalPoint') {
        const { focalPoint, image } = fields

        return {
            fields: image?.fields,
            focus: focalPoint?.focalPoint,
        }
    }

    return {
        fields,
        focus: false,
    }
}

const getFocusPercent = (size, focus) => {
    if (!size || !focus || size <= 0 || focus <= 0) return 50
    return round(focus / (size / 100))
}

const ContentfulImage = ({
    className,
    outerClassName,
    asset,
    sizes,
    ratio,
    widths,
    type,
    useRatio,
    ...props
}) => {
    const [ref, inView] = useInView({
        triggerOnce: true,
        rootMargin: '200px',
    })

    const { fields, focus } = getImgFields(asset)
    const { description = '', title = '', file = {} } = fields
    const imgSrc = file?.url || ''
    const alt = description || title || ''

    const generateURL = (width, format) => {
        const search = new URLSearchParams()

        search.append('fm', format)
        search.append('w', width)

        return `${imgSrc}?${search.toString()}`
    }

    const getRatio = () => {
        if (ratio) return ratio
        const { width = 16, height = 9 } =
            asset?.fields?.file?.details?.image || {}
        return round((height / width) * 100, 100)
    }

    const style = props.style || {}

    if (!!focus) {
        const { width, height } = file?.details?.image
        const x = getFocusPercent(width, focus.x)
        const y = getFocusPercent(height, focus.y)
        style.objectPosition = `${x}% ${y}%`
    }

    return (
        <picture
            className={mergeClassNames(
                s.container,
                useRatio && s.ratio,
                outerClassName,
            )}
            style={useRatio ? { paddingBottom: `${getRatio()}%` } : null}
        >
            {[`webp`, type].map((type) => {
                const srcSet = widths
                    .map((width) => `${generateURL(width, type)} ${width}w`)
                    .join(`,`)

                return (
                    <source
                        key={type}
                        type={`image/${type}`}
                        sizes={sizes}
                        srcSet={inView ? srcSet : null}
                    />
                )
            })}

            <img
                className={mergeClassNames(className, s.imgEl)}
                src={inView ? generateURL(1024, type) : null}
                ref={ref}
                alt={alt}
                {...props}
                style={style}
            />
        </picture>
    )
}

ContentfulImage.propTypes = {
    asset: PropTypes.object.isRequired,
    /** The attribute of <source> */
    sizes: PropTypes.string,
    useRatio: PropTypes.bool,
    /** The widths of srcset */
    widths: PropTypes.arrayOf(PropTypes.number),
    type: PropTypes.oneOf([`png`, `jpg`]),
}

ContentfulImage.defaultProps = {
    asset: ``,
    sizes: `100vw`,
    useRatio: true,
    widths: [64, 128, 256, 384, 512, 768, 1024, 1280, 1536],
    type: `jpg`,
}

export default ContentfulImage
