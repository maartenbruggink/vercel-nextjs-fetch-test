import { handleRedirect } from '_utils/server/redirects'

const Index = () => <div />

export async function getServerSideProps({ res, req }) {
    return await handleRedirect(req, res)
}

export default Index
