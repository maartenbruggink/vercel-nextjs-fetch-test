import { motion } from 'framer-motion'

import { getEntries } from '_utils/functions/contentfulClient'
import { getLocale } from '_utils/functions/geoHelpers'
import { handleRedirect } from '_utils/server/redirects'

import Head from '_components/base/Head'
import Footer from '_components/base/Footer'
import PageLink from '_components/links/PageLink'

import s from './about.module.scss'
import ContentfulImage from '_components/media/ContentfulImage'

const Index = ({ page }) => {
    return (
        <>
            <Head title={page.title} />
            <motion.article
                className={s.container}
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                exit={{ opacity: 0 }}
            >
                <h1>{page.title}</h1>

                <ContentfulImage asset={page.headerImage} />

                <PageLink page="/about">
                    <a>To About</a>
                </PageLink>
            </motion.article>
            <Footer />
        </>
    )
}

export async function getServerSideProps({ req, res, params }) {
    const locale = getLocale(params)
    if (!locale) return await handleRedirect(req, res)

    const {
        items: [{ fields }],
    } = await getEntries({
        content_type: 'homepage',
        locale,
    })

    const { title, headerImage } = fields || {}
    return { props: { page: { title, headerImage } } }
}

export default Index
