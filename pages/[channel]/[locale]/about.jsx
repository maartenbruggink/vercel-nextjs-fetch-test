import { getEntries } from '_utils/functions/contentfulClient'
import { getLocale } from '_utils/functions/geoHelpers'
import { handleRedirect } from '_utils/server/redirects'

import Head from '_components/base/Head'
import Footer from '_components/base/Footer'
import PageLink from '_components/links/PageLink'

import s from './about.module.scss'
import { motion } from 'framer-motion'

const About = ({ page }) => {
    return (
        <>
            <Head title={page.title} />
            <motion.article
                className={s.container}
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                exit={{ opacity: 0 }}
            >
                <h1>{page.title}</h1>
                <p>{page.aboutUs}</p>

                <PageLink>
                    <a>To Home</a>
                </PageLink>
            </motion.article>
            <Footer />
        </>
    )
}

export async function getServerSideProps({ req, res, params }) {
    const locale = getLocale(params)
    if (!locale) return await handleRedirect(req, res)

    const {
        items: [{ fields }],
    } = await getEntries({
        content_type: 'about',
        locale,
    })

    const { title, aboutUs } = fields || {}
    return { props: { page: { title, aboutUs } } }
}

export default About
