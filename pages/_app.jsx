import { useState } from 'react'
import NextApp from 'next/app'
import { SWRConfig } from 'swr'
import { AnimatePresence } from 'framer-motion'
import { InstantSearch } from 'react-instantsearch-dom'

import {
    algoliaClient,
    getIndiceName,
    getAlgoliaNavigationTree,
} from '_utils/functions/algoliaClient'
import { getLocales, getEntries } from '_utils/functions/contentfulClient'
import { getLocale } from '_utils/functions/geoHelpers'
import { formatTranslationStrings } from '_utils/functions/dataFormatting'
import { channels } from '_utils/config/geo'

import DataWrapper from '_components/base/DataWrapper'
import Menu from '_components/menu/Menu'

import '_styles/main.scss'
import '_styles/algolia.scss'

const defaultFetcher = async (...args) => {
    const data = await fetch(...args)
    return await data.json()
}

const App = ({ siteData, Component, pageProps, router }) => {
    const [savedSiteData] = useState(siteData || {})
    const { cfLocale, locales, menu, translationStrings } = savedSiteData

    const dataLayer = { channels, locales, translationStrings }

    return menu ? (
        <DataWrapper {...dataLayer}>
            <SWRConfig fetcher={defaultFetcher}>
                <Menu {...menu} />
                <AnimatePresence exitBeforeEnter>
                    <InstantSearch
                        searchClient={algoliaClient}
                        indexName={getIndiceName(
                            router?.query?.channel,
                            router?.query?.locale,
                        )}
                    >
                        <Component
                            key={router.route}
                            locale={cfLocale}
                            {...pageProps}
                        />
                    </InstantSearch>
                </AnimatePresence>
            </SWRConfig>
        </DataWrapper>
    ) : (
        <div />
    )
}

App.getInitialProps = async (appContext) => {
    const { ctx, router } = appContext
    const { asPath = '' } = router || {}

    const jsonRequest = asPath.includes('.json')

    let siteData

    if (!jsonRequest) {
        const locale = getLocale(ctx.query)

        const requests = [
            getLocales(),
            getEntries({
                content_type: 'siteNavigation',
                locale,
            }),
            getEntries({
                content_type: 'navigationShop',
                locale,
            }),
            getEntries({
                content_type: 'resourceSet',
                locale,
            }),
            getAlgoliaNavigationTree(ctx.query),
        ]
        const [
            { items: locales },
            {
                items: [navSite],
            },
            {
                items: [navShop],
            },
            { items: resourceSets = [] },
            { menuFilters },
        ] = await Promise.all(requests)

        siteData = {
            cfLocale: locale,
            locales,
            channels,
            menu: { navSite, navShop, menuFilters },
            translationStrings: formatTranslationStrings(resourceSets),
        }
    }
    const appProps = await NextApp.getInitialProps(appContext)

    return {
        ...appProps,
        siteData,
    }
}

export default App
